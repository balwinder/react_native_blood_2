import React ,{Component} from 'react';
import {View,Text,TextInput,Button,StyleSheet,FlatList,Dimensions,TouchableHighlight} from 'react-native';
import GridView from "../../component/GridView/gridView";
import {connect} from 'react-redux';
import {getPlace} from '../../store/actions/index'
import PickLocation from '../../component/PickLocation/PickLocation'


class chooseBlood extends Component{

   state={
       coordinates:{
        longitude:"",
        latitude:""
       },
  
    bloodGroup :
    [
        {
            id:1,
            value : "OPOSITIVE",
            symbol : "O+"
        },
        {
            id:2,
            value : "ONAGATIVE",
            symbol : "O-"
        },
        {
            id:3,
            value : "ABPOSITIVE",
            symbol : "AB+"
        },
        {
            id:4,
            value : "ABNAGATIVE",
           symbol : "AB-"
        },
        {
            id:5,
            value : "APOSITIVE",
            symbol : "A+"
        },
        {
            id:6,
            value : "ANAGATIVE",
            symbol : "A-"
        },
        {
            id:7,
            value : "BPOSITIVE",
            symbol : "B+"
        },
        {
            id:8,
            value : "BNAGATIVE",
            symbol : "B-"
        },

    ]
        
    

    // arrayOfObjects : [{"id":28,"Title":"Sweden"}, {"id":56,"Title":"USA"}, {"id":89,"Title":"England"}]
   }

    constructor(props){
        super(props);
        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent);
        this.getLocationHandler() ; 
    }
 

    itemSelecterdHandlert =(item)=>{
  
 this.getLocationHandler() ;  

const cordinates=this.state.coordinates
        this.props.navigator.push({
            screen :"awesome-places.FindPlace",
           // title:selPlace.name,
            passProps:{
                bloodGroup:item,
                coordinate:cordinates
            }
        })
     
       
    }
    pickLocationHandler = event =>{       
        const coords = event.nativeEvent.coordinate
        this.setState(prevState=>{
            return{
                ...prevState,
                coordinates:{
                    longitude:coords.longitude,
                    latitude:coords.latitude      
                }
            }
        })
        }
        
       getLocationHandler =()=>{      
          navigator.geolocation.getCurrentPosition(pos =>{
            const coordsEvents = {
               nativeEvent:{
                   coordinate:{
                       longitude:pos.coords.longitude,
                       latitude:pos.coords.latitude
                   }
               }
           }
        this.pickLocationHandler(coordsEvents)
         },
        err=>{
            console.log(err);
            alert("Fething fail pPlkease pick manually")
        }
    )
    
     }
    
  render()
   {
        return (
            <FlatList
            numColumns={2}
            data ={this.state.bloodGroup
              //,"OPOSITIVE","APOSITIVE","ANAGATIVE","BPOSITIVE","BNAGATIVE","ABPOITIVE","ABNAGATIVE"
            } 
            renderItem={({item})=>{    
                return <GridView 
                 data1 ={item.symbol} 
                 touch1 ={()=>this.itemSelecterdHandlert(item.value)}/>
                 
            }}
         

            keyExtractor={item => item.id}
            
            />
          
        )
    }
}
const mapStateToProps = state => {
    console.log(state)
    return{
        email:state.auth.email
    };
  }

export default connect(mapStateToProps,null)(chooseBlood);


