import React ,{Component} from 'react';
import {View,Text,TouchableOpacity,StyleSheet,Animated} from 'react-native';
import {connect} from 'react-redux';

import {Button} from 'react-native'
import {getMessage} from '../../store/actions/index'
import PlaceListAlreadyChat from '../../component/PlaceListAlreadyChat/PlaceListAlreadyChat'
import Authdata from '../../screens/Auth/Auth'
import AuthScreen from '../../screens/Auth/Auth';

class AlreadyInChat extends Component{
    static navigatorStyle ={
        navBarButtonColor:"orange"
    }
    state={
        coordinates:[{
            longitude:null,
            latitude:null
        
           }],
        placesLoaded:false,
        removeAnim: new Animated.Value(1),
        placesAnim: new Animated.Value(0),
        coordinate:{
            longitude:null,
            latitude:null
        }
    }
    placesLoadedHandler = () => {
       
        Animated.timing(this.state.placesAnim, {
          toValue: 1,
          duration: 500,
          useNativeDriver: true
        }).start();
      };
       placesSearcHandler =()=>{
          Animated.timing(this.state.removeAnim,{
              toValue:0,
              duration:500,
              useNativeDriver:true,
              
          } ).start(() => {
            this.setState({
              placesLoaded: true,
              
            });
           
            
           // this.props.onLoadPlaace(this.props.bloodGroup,this.props.coordinate);
        })
       }
     constructor(props){
        super(props);
        this.props.onLoadMessage(this.props.email);
        //console.log(this.props.message)\
       
     }
    
   onNavigatorEvent = event =>{
    if(event.type==="NavBarButtonPress"){
        if(event.id==="sideDrawerToggle"){
            this.props.navigator.toggleDrawer(
                {
                    side:"left"
                }
            );
        }
    }
 }
 

  itemSelecterdHandlert =(key)=>{
      console.log("yes")
        console.log(this.props.message[0].email1)
      const selPlace =this.props.message.find(place=>{
          return place.key ===key;
      });
      this.setState({
          placesLoaded: false
        });
      this.props.navigator.push({
          screen :"awesome-places.Chat",
          title:this.props.message[0].email1,
          passProps:{
              selectedPlace:selPlace,
              reciverEmail:this.props.message[0].email1
          }
      })
  }
render (){
  let content =(
      <Animated.View 
      style={{
          opacity:this.state.removeAnim,
          transform:[
              {
                  scale: this.state.removeAnim.interpolate({
                      inputRange: [0,1],
                      outputRange:[12,1]
                  }     
                  )
              }
          ]
      }}>
         <TouchableOpacity onPress= {this.placesSearcHandler}>
         <View style={styles.searchButton} >
             <Text style={styles.searchButtonText}> heloo</Text>
             </View>
         </TouchableOpacity>
         </Animated.View>
  );
  if(this.state.placesLoaded){
      content=(
          <PlaceListAlreadyChat places={this.props.message} 
          onItemSelected ={this.itemSelecterdHandlert}/>
      )
  }
  return(
     <View style={this.state.placesLoaded?null:styles.buttonContainer}>
         {content}
     </View>
  );
}
} 
const styles = StyleSheet.create({
  buttonContainer:{
   flex:1,
   justifyContent:"center",
   alignItems:"center"
  },
  searchButton:{
      borderColor:"orange",
      borderWidth:3,
      borderRadius:50,
      padding:20
  },
  searchButtonText:{
      color:"orange",
      fontWeight:"bold",
      fontSize:26
  }
})
     

// const mapStateToProps = state => {
//     return{
//         places:state.places.places
//     };
// }
// const mapDispatchToProps =dispatch =>{
//     return{
//         onLoadPlaace:(bloodGroup,coordinate)=>dispatch(getPlace(bloodGroup,coordinate))
//     }
// }
const mapStateToProps = state => {
    console.log("jhooo")
    console.log(state)
    return{
        message:state.Message.Message,
        email:state.auth.email
    };
}
const mapDispatchToProps =dispatch =>{
    return{
        onLoadMessage:(email)=>dispatch(getMessage(email))
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(AlreadyInChat);
