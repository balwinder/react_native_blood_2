import React,{Component} from 'react';
import {View,Text,Dimensions,StyleSheet,Button,TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons'
import {Platform} from "react-native"

class SideDrwaer extends Component{
    render(){
        return(
            <View style={[styles.container,{width:Dimensions.get("window").width*0.8}]}>
            <TouchableOpacity>
            <View style={styles.draweItem}>
                <Icon name={Platform.OS==="android"?"md-log-out":"ios-log-out"} size={30} color="#aaa" style={styles.drawerItemIcon}/>
               <Text >SideDrawe</Text>
               </View>
                </TouchableOpacity>
                </View>
        );
    }
}
const styles = StyleSheet.create({
    container:{
         padding:50,
         backgroundColor:"white",
         flex:1 
    },
    draweItem:{
        flexDirection:"row",
        alignItems:"center",
        padding:10,
        backgroundColor:"#eee",
        justifyContent:"space-around"

    },
    drawerItemIcon:{
     marginRight:10
    }
    
})
export default SideDrwaer; 