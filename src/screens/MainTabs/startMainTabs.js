import { Navigation} from 'react-native-navigation';
import Icon from 'react-native-vector-icons/Ionicons';
import {Platform} from 'react-native';
const startTabs =()=>{
    Promise.all([
        Icon.getImageSource(Platform.OS==="ios"?"ios-map":"md-map",30),
        Icon.getImageSource(Platform.OS==="ios"?"ios-share":"md-share-alt",30),
        Icon.getImageSource(Platform.OS==="ios"?"ios-menu":"md-menu",30)

    ]).then(sources =>{
        Navigation.startTabBasedApp({
            tabs:[
                {
                    screen:"awesome-places.ChooseBloodGroup",
                    label:"Choose Blood Group",
                    title:"Choose Blood Group",
                    icon:sources[0],
                    navigatorButtons:{
                        leftButtons:[
                            {
                            icon:sources[2],
                            title:"Menu",
                            id:"sideDrawerToggle"
                            }
                        ]
                    }
                },
                {
                    screen:"awesome-places.AlreadyInChat",
                    label:"Chat",
                    title:"Chat",
                    icon:sources[1],
                    navigatorButtons:{
                        leftButtons:[
                            {
                            icon:sources[2],
                            title:"Menu",
                            id:"sideDrawerToggle"
                            }
                        ]
                    }
                },
                {
                    screen:"awesome-places.ChooseBloodGroup",
                    label:"Share Place",
                    title:"Share Place",
                    icon:sources[1],
                    navigatorButtons:{
                        leftButtons:[
                            {
                            icon:sources[2],
                            title:"Menu",
                            id:"sideDrawerToggle"
                            }
                        ]
                    }
                }
                
                
            ],
            tabsStyle:{
                tabBarSelectedButtonColor:"orange"

            },
            drawer:{
                left:{
                    screen:"awesome-places.SideDrawr"
                }
               
                
            },
            appStyle:{
                tabBarSelectedButtonColor:"orange"
            }
        })
    })
   

};
export default startTabs;