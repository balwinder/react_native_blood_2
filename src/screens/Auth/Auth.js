import React ,{Component} from 'react';
import {View,Text,Button,TextInput,StyleSheet,ImageBackground,Dimensions,KeyboardAvoidingView,
Keyboard,ActivityIndicator,Picker,PickerIOS,
  TouchableWithoutFeedback} from 'react-native';
  
import DefaultInput from "../../component/UI/DefaultInput/DefaultInput"
import HeadingText from "../../component//UI/HeadingText/HeadingText";
import MainText from "../../component//UI/MainText/MainText";
import backgoundimage from "../../assets/mountain.jpg"
import ButtonWithBackground from "../../component/UI/ButtonWithBackground/ButtonWithBackground";
import validate from '../../utility/validation';
import {connect} from 'react-redux'
import {tryAuth} from '../../store/actions/index'
import DropDownList from "../../component/DropDownSpinner/dropdownSpinner";
import { Dropdown } from 'react-native-material-dropdown';
import { YellowBox } from 'react-native';
YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader']);
class AuthScreen extends Component{
    state ={
    viewMode:Dimensions.get("window").height>500?"portrait":"landscape",
    authMode:"Login",
    constrols:{
        email:{
            value:"",
            valid:false,
            validationRule:{
                isEmail:true
            },
            touched:false

        },
        password:
        {
            value:"",
            valid:false,
            validationRule:{
            minLength:6
        },
        touched:false
        },
        confirmPassword:{
            
            value:"",
            valid:false,
            validationRule:{
            equalTo:'password'
        },
        touched:false
    }
    },
    PickerSelectedVal:null,
    coordinate:{
        longitude:"",
        latitude:""
       
        
    }
    

    }
    getSelectedPicker =() =>{
       Alert.alert("Selected Country is:"+this.state.PickerSelectedVal )
    }
   


    getLocationHandler =() =>{
        navigator.geolocation.getCurrentPosition(pos =>{
          const coordsEvents = {
              nativeEvent:{
                  coordinate:{
                      longitude:pos.coords.longitude,
                      latitude:pos.coords.latitude
                  }
              }
              
          }
         
          this.pickLocationHandler(coordsEvents)
          console.log("hello3")
        },
       err=>{
           console.log(err);
           alert("Fething fail pPlkease pick manually")
       }
       
   )

    }

    pickLocationHandler = event =>{
        const coords = event.nativeEvent.coordinate
        this.setState(prevState=>{
            return{
                ...prevState,
                coordinate:{
                    longitude:coords.longitude,
                    latitude:coords.latitude      
                }
            }
        })
       
          
        }
    



    updateInputState = (key,value) =>{
       
        let connectedValue={};
        if(this.state.constrols[key].validationRule.equalTo){
            const equalControl=this.state.constrols[key].validationRule.equalTo;                    
            const equalValue =this.state.constrols[equalControl].value;
            console.log(equalValue);
            connectedValue={
                ...connectedValue,
                equalTo:equalValue               
            }
        }
        if(key==='password'){
            
            //console.log(equalValue);
            connectedValue={
                ...connectedValue,
                equalTo:value               
            }

        }
    this.setState(prevState =>{
        return{ 
            constrols:{
                
                ...prevState.constrols,
                confirmPassword:{
                    ...prevState.constrols.confirmPassword,
                    valid: key==='password'? validate(prevState.constrols.confirmPassword.value,
                        prevState.constrols.confirmPassword.validationRule,
                        connectedValue): 
                    prevState.constrols.confirmPassword.valid
                },
                [key]:{
                    ...prevState.constrols[key],
                    value:value, 
                    valid :validate(value,prevState.constrols[key].validationRule,connectedValue) ,
                    touched:true              
                }
            }
        } 
    })
    }
constructor (props){
    super(props);
    Dimensions.addEventListener("change",this.updateStyle)
    this.getLocationHandler();
}
componentWillUnmount(){
    Dimensions.removeEventListener("change",this.updateStyle);

}
swithAuthModeHandler = () =>{
    this.setState(prevState=>{
        return{
            authMode:prevState.authMode ==="Login"?"signup":"Login"
        }
    });
}

updateStyle =(dims) =>{
    this.setState({
        viewMode:dims.window.height>500?"portrait":"landscape"
       })
}
    authHandler=() =>{
        const authData ={
            email:this.state.constrols.email.value,
            password:this.state.constrols.password.value,
            bloodGroup:this.state.PickerSelectedVal,
            coordinate:this.state.coordinate
        }
        this.props.onTryAuth(authData,this.state.authMode)
      
    
    }
    render(){
        let data ={bloodgroup:[{
            label:"A+",
             value:"APOSITIVE",
          }, 
          {
            label:"A-",
             value:"ANAGATIVE",
          },
          {
            label:"B-",
             value:"BNAGATIVE",
          },
          {label:"B+",
           value:"BPOSITIVE" },
          {label:"AB+",
           value:"ABPOSITIVE" },
        {label:"AB-",
         value:"ABNAGATIVE" },
         { label:"O+" ,
         value:"OPOSITIVE" },
         {label:"O-",
          value:"ONAGATIVE"} 

        ]};
          


        let confirmPasswordControl = null;
        let dropdown = null;
        let headingText =null;
        console.log("bh: "+this.state.PickerSelectedVal)
        let submitButton =(
            <ButtonWithBackground color="#29aaf4" onPress={this.authHandler}
            disable ={!this.state.constrols.email.valid 
                ||!this.state.constrols.password.valid               
                ||(!this.state.constrols.confirmPassword.valid ||this.state.PickerSelectedVal==null)&&
                this.state.authMode === "signup" 
                 } >
               Submit
            </ButtonWithBackground>
        )
        if(this.state.viewMode==="portrait"){
            headingText=
        (<MainText>
        <HeadingText>Please Log In</HeadingText>
        </MainText>)
        }
        if (this.state.authMode==='signup'){
            confirmPasswordControl =(<View style={
                this.state.viewMode==="portrait"
                ?style.portraitPasswordWrapper
                :style.landscapPasswordWrapper
                }>
            <DefaultInput placeholder="Confirm Password" 
            style={style.input}
            value={this.state.constrols.confirmPassword.value}
            onChangeText={(val)=>this.updateInputState('confirmPassword',val)}
            valid = {this.state.constrols.confirmPassword.valid}
            touched ={this.state.constrols.confirmPassword.touched}
             secureTextEntry/>
               </View>)
                dropdown = (<View style={style.listItem } >
                <Dropdown
           label='Favorite Fruit'       
          data={data.bloodgroup}
           itemTextStyle={{marginLeft:120,fontSize: 30,
           fontWeight: 'bold',padding:5}}
    
     onChangeText={(data) => this.setState({PickerSelectedVal: data})}
     
   />
                    {/* <Picker  
         selectedValue={this.state.PickerSelectedVal}
       
         onValueChange={(itemValue, itemIndex) => this.setState({PickerSelectedVal: itemValue})}
       
       >
       
       <Picker.Item label="Select Blood Group" />
       <Picker.Item label="A+" value="APOSITIVE" />
       <Picker.Item label="B+" value="BPOSITIVE" />
       <Picker.Item label="AB+" value="ABPOSITIVE" />
       <Picker.Item label="AB-" value="ABNAGATIVE" />
       <Picker.Item label="O+" value="OPOSITIVE" />
       <Picker.Item label="O-" value="ONAGATIVE" />
       </Picker>  */}
       </View>)
        }
      
        if(this.state.isLoading){
            submitButton =<ActivityIndicator />
        }
    
        return(
     <ImageBackground source ={backgoundimage} style={style.backgoundImage}>
  
 <KeyboardAvoidingView 
   style= {style.container} 
   behavior="padding"
>
{headingText}

    <ButtonWithBackground 
    onPress ={this.swithAuthModeHandler}
    color="#29aaf4"> 
       switch to {this.state.authMode==='Login'?'Sign up':"Login"}
    </ButtonWithBackground>
  <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
    <View style={style.inputContainer}> 
    <DefaultInput placeholder="Your Email Address" 
    style={style.input}
    value={this.state.constrols.email.value}
    onChangeText={(val)=>this.updateInputState('email',val)}
    valid = {this.state.constrols.email.valid}
    touched ={this.state.constrols.email.touched}
    autoCapitalize="none"
    autoCorrect={false}
    keyboardType="email-address"/>
    <View style={
        this.state.viewMode==="portrait"
        ||this.state.authMode==="Login"
        ?style.portratePasswordContaner
        :style.landScapPasswordContaner}>
    <View style={
        this.state.viewMode==="portrait"
        ||this.state.authMode==="Login"
        ?style.portraitPasswordWrapper
        :style.landscapPasswordWrapper}>
    <DefaultInput placeholder="Password" 
    style={style.input}
    value={this.state.constrols.password.value}
    onChangeText={(val)=>this.updateInputState('password',val)}
    valid = {this.state.constrols.password.valid}
    touched ={this.state.constrols.password.touched}
    secureTextEntry/>
    </View>
   
    {confirmPasswordControl}
   
    </View>
    <View>
         
        </View>
        {dropdown}
    </View>
    </TouchableWithoutFeedback>   
    {submitButton}
    </KeyboardAvoidingView>
   </ImageBackground>
        ) 
    }
}
const style = StyleSheet.create({
    container :{
        flex:1,
        justifyContent:"center",
        alignItems:"center"  
    },
    backgoundImage:{
        width:"100%",
        flex:1
    },
    inputContainer:{
        width:"80%"
    },
    input:{
      backgroundColor:"#eee",
      borderColor:"#bbb"
    },
    landScapPasswordContaner:{
        flexDirection:"row",
        justifyContent:"space-between"
    },
    portratePasswordContaner:{
        flexDirection:"column",
        justifyContent:"flex-start"
    },
    landscapPasswordWrapper:{
        width:"45%"
    },
    portraitPasswordWrapper:{
        width:"100%"
    },
    listItem:{
        
        justifyContent:"center",
       
        marginTop:8,
         marginBottom:8,        
         width:"100%",
         padding:5,
         height:45,
         borderWidth:2,
        borderColor:"#eee",
       backgroundColor:"white",
        }
})
const stateToProps=state =>{
    return{
        isLoading:state.ui.isLoading
    }
   
} 
const mapDispatchToProps = dispatch=>{
   
    return {
        onTryAuth:(authData,authMode)=> dispatch(tryAuth(authData,authMode))
    }
}

export default connect(stateToProps,mapDispatchToProps)(AuthScreen);