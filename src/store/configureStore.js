import {createStore,combineReducers,compose,applyMiddleware} from 'redux';
import thunk from 'redux-thunk'
import placeReducer from './reducers/places'
import uiReducer from './reducers/ui'
import authReducer from "./reducers/auth"
import MessageReducer from './reducers/Messages'
const rootReducer = combineReducers({
    places:placeReducer,
    ui:uiReducer,
    auth:authReducer,
    Message:MessageReducer
});
let composeEnhancers = compose;
if(__DEV__)
{
    composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
}

    const configureStore =()=>{
    return createStore(rootReducer,composeEnhancers(applyMiddleware(thunk)));
}
export default configureStore; 