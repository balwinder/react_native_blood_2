
import{SET_PLACES} from "./actionTypes"
import {uiStartLoading,uiStopLoading,authGetToken} from './index'
export const addPlace =(placeName,location,image)=>{ 
   
    return dispatch =>{    
        dispatch(uiStartLoading());
        const placeData = {
            name:placeName,
            location:location,
            productImage:image.base64           
        }; 
        dispatch(authGetToken())
        .catch(()=>{
            alert ("No valid Token")
        }) .then(token =>{
            return  fetch("http://localhost:5000/production",{
                method:"POST",
                headers : {
                    'Content-Type': 'application/json',
                    'Authorization' : "bb "+token
                },
                body:JSON.stringify(placeData)
            }) 
        })
        .catch(()=>{
            alert("No token found")
        })
        
        .then(res=>res.json())
        .then(parsedRes =>{
           
            dispatch(uiStopLoading());
           
        }).catch(err=>{
            console.log(err)
            alert("Something went wrong")
            dispatch(uiStopLoading());
        });
    }; 
}
export const getPlace = (bloodGroup,coordinate) =>{
  
    return dispatch=>{ 
        const cordinate = {
            coordinate:coordinate
        }; 
        dispatch(authGetToken())
        .catch(()=>{
           // alert ("No valid Token")
        })
        .then(token =>{
            return  fetch("http://localhost:5000/user/"+bloodGroup+"/?lat="+coordinate.latitude+"&lng="+coordinate.longitude,{
                method:"GET",
                headers : {
                    'Content-Type': 'application/json',
                    'Authorization' : "bb "+token
                }
            }) 
        })
        .catch(()=>{
alert("No token found")
          }).then(res=>res.json())
          .then(parsedRes=>{
              const places=[]
              for(let key in parsedRes){
                  places.push({
                      ...parsedRes[key],
                    //    image:{
                    //        uri:parsedRes[key].image
                    //       },
                          key:key
                  });            
              }      
         dispatch(setPlace(places))
          }).catch(err=>{
              alert("something went wrong")
              console.log(err)
             
          })

    }
}
export const setPlace = places =>{
    
    return{
        type:SET_PLACES,
        places:places
    }
}

export const deletePlace =(key,imageLink)=>{
console.log(key)
    return dispatch=>{   
        dispatch(authGetToken())
        .catch(()=>{
            alert ("No valid Token")
        })   .then(token =>{
            return  fetch("http://localhost:5000/production/"+key,{
                method:"DELETE",
                headers : {
                    'Content-Type': 'application/json',
                    'Authorization' : "bb "+token
                },
                body:JSON.stringify(imageLink)
            }) 
        }).catch(()=>{
            alert("No token found")
                      })
       
        .then(res=>res.json()).catch(err=>{
            alert("something went wrong")
            console.log(err)
           
        })
        
    }
}