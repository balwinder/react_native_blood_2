import React,{Component} from 'react';
import {View, Text,StyleSheet,Picker,PickerIOS} from 'react-native';

class dropDownlist extends Component {
    state = {
        PickerSelectedVal: " "
    };
    getSelectedPicker =() =>{
        console.log("Selected Country is:"+this.state.PickerSelectedVal )
     }
    render() {
      
        return (
    
  
<View  style={styles.listItem } >
<Picker  
  selectedValue={this.state.PickerSelectedVal}
  onValueChange={(itemValue, itemIndex) => this.setState({PickerSelectedVal: itemValue})}
  
>

<Picker.Item label="A+" value="APOSITIVE" />
<Picker.Item label="B+" value="BPOSITIVE" />
<Picker.Item label="AB+" value="ABPOSITIVE" />
<Picker.Item label="AB-" value="ABNAGATIVE" />
<Picker.Item label="O+" value="OPOSITIVE" />
<Picker.Item label="O-" value="ONAGATIVE" />

</Picker> 
</View>
    
)


}}

const styles = StyleSheet.create({ 
    listItem:{
     
    justifyContent:"center",
    marginTop:8,
     marginBottom:8,
     height: 35,
     width:"100%",
     padding:5,
     borderWidth:2,
    borderColor:"#eee",
   backgroundColor:"white",

  
    }
    // width:"100%",
    // borderWidth:1,
    // borderColor:"#eee",
    // padding:5,
    // marginTop:8,
    // marginBottom:8
});
export default dropDownlist;