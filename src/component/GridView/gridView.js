import React,{Component} from 'react'
import {View,Text,StyleSheet,Animated,TouchableWithoutFeedback,Dimensions,Card} from 'react-native'
import {getPlace} from '../../store/actions/index'
class GridView extends Component{

    state ={
        animatePress : new Animated.Value(1)
    }
   
    heloo(){
        console.log("helo darling")
    }
    animatedin(){
        Animated.timing(this.state.animatePress,{
            toValue:0.8,
            duration:200
        }).start()
    }
    animateOut(){
        Animated.timing(this.state.animatePress,{
            toValue:1,
            duration:200
        }).start()
    }
    render (){
        const ITEM_WIDTH = Dimensions.get('window').width
        return(
           
            <TouchableWithoutFeedback
            onPressIn={()=>this.animatedin()}
            onPressOut={()=>this.animateOut()}
            onPress ={this.props.touch1}
            >
         <Animated.View style = {[{
             margin:5,
             transform:[
                 {
                     scale:this.state.animatePress
                 }
             ]
         },styles.container,{width:(ITEM_WIDTH-20)/2}]}>
         
            
   <Text style ={styles.text}>{this.props.data1}</Text>


         </Animated.View>
       
         </TouchableWithoutFeedback>
       
            
        )
    
}
}


const styles = StyleSheet.create({
    container:{
      
   justifyContent:"center",
    height: 100,
   
//     marginTop:8,
//      marginBottom:8,
//      height: 35,
//      width:"100%",
//      padding:5,
//      borderWidth:2,
//     borderColor:"#eee",
    backgroundColor:"red",
    },
    text:{
        fontSize:28,
        fontWeight:"bold",
       marginLeft:80,
    },
    view:{
        marginTop:100
    }
})

export default GridView;
