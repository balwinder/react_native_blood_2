import React from 'react';
import {FlatList, Text,StyleSheet} from 'react-native';
import ListItem from '../Listitem/ListItem';

const placeListAlreadyChat = props =>{

      return (
          <FlatList style ={styles.listContainer}
          data ={props.places}
          renderItem={(info)=>(
            
            <ListItem Email = {info.item.email1} 
            placeImage = {info.item.image}
            onItemPressed = {()=>props.onItemSelected(info.item.key)}/>
          )}/>
      )
}
const styles = StyleSheet.create({
    listContainer:{
        width:'100%'     
      }
})
export default placeListAlreadyChat;